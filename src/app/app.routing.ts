import {ModuleWithProviders} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';

//import artist
import {UserEditComponent} from './components/user-edit.component';
import {UsandoLibreriaComponent} from './components/usandoLibreria.component';
import{ArtistListComponent} from './components/artist-list.component';
import{ArtistAddComponent} from './components/artist-add.component';
import{ArtistEditComponent} from './components/artist-edit.component';
import{ArtistDetailComponent} from './components/artist-detail.component';
import{AlbumAddComponent} from './components/album-add.component';
import{AlbumEditComponent} from './components/album-edit.component';

// Home
import{HomeComponent} from './components/home.component';
import{ClaseCarreraExplorarComponent} from './components/claseCarreraExplorar.component';
//Test
import{MisTestComponent} from './components/mis-test.component';
import{AgregarPreguntaComponent} from './components/agregar-pregunta.component';
import{ConfigurarTestComponent} from './components/configurar-test.component';
import{EditarPreguntaComponent} from './components/editar-pregunta.component';
import{MisCarrerasComponent} from './components/misCarreras.component';
import{MisCarrerasDetailComponent} from './components/miCarrera-detail.component';
//Otras cosas
import{MisTestOficialComponent} from './components/mis-test-oficial.component';
import{AgregarPersonalidadComponent} from './components/agregar-personalidad.component';
import{PanelControlAlumnosComponent} from './components/panel-control-alumnos.component';
import{AdministracionUsuariosComponent} from './components/administracion-usuarios.component';
const appRoutes:Routes=[
    // {
    // path:'',
    // redirectTo:'/artist/1',
    // pathMatch:'full'
    // },
    // {path:'',component:HomeComponent},
    // {path:'',component:ArtistListComponent},
    {path:'home',component:HomeComponent},
    {path:'mis-carreras',component:MisCarrerasComponent},
    {path:'mi-carrera/3',component:MisCarrerasDetailComponent},
    {path:'clase-explorar/4',component:ClaseCarreraExplorarComponent},
    {path:'artistas/:page',component:ArtistListComponent},
    {path:'crear-artista',component:ArtistAddComponent},
    {path:'editar-artista/:id',component:ArtistEditComponent},
    {path:'artista/:id',component:ArtistDetailComponent},
    {path:'crear-album/:artista',component:AlbumAddComponent},
    {path:'editar-album/:id',component:AlbumEditComponent},
    {path:'mis-datos',component:UserEditComponent},
    {path:'mis-graficas',component:UsandoLibreriaComponent},
    {path:'mi-test',component:MisTestComponent},
    {path:'configurar-test/:page',component:ConfigurarTestComponent},
    {path:'crear-pregunta',component:AgregarPreguntaComponent},
    {path:'editar-pregunta/:id',component:EditarPreguntaComponent},
    {path:'mis-test-oficial',component:MisTestOficialComponent},
    {path:'agregar-personalidad',component:AgregarPersonalidadComponent},
    {path:'analitycs',component:PanelControlAlumnosComponent},
    {path:'administracion-usuarios',component:AdministracionUsuariosComponent},
    {path:'**',component:MisTestOficialComponent},

];


export const appRoutingProviders:any[]=[];
export const routing:ModuleWithProviders=RouterModule.forRoot(appRoutes);





