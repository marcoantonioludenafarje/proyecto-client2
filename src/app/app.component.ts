import { Component,OnInit } from '@angular/core';
import {UserService} from './services/user.service';
import { User } from './models/user';
import {GLOBAL} from './services/global';
import { WindowRef } from './services/windowRef';
import { UserFinal } from './models/userfinal';
import {Router,ActivatedRoute,Params} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers:[UserService,WindowRef]
})
export class AppComponent implements OnInit {
  public title='TARMI';
  // public titleKendo='Hello World !';
  public user:User;
  public user_register:User;
  public userFinal;
  public identity;
  public token:String;
  public errorMessage;
  public errorMessageRegister;
  public errorMessageRegisterFacebook;
  public url:string;
  public authenticacion;
  public datosBasicos;

  //Estos son lo de mi formulario
  public mostrarTodo;
  public elementos:any;
  public opciones:any;
  public terminos:any;
  public ocultar=false;

      constructor(
        private _userService:UserService,
        private _route:ActivatedRoute,
        private _router:Router

      ){
        this.user =new User('','','','','','ROLE_USER','');
        this.user_register=new User('','','','','','ROLE_USER','');
                this.url=GLOBAL.url;
        
      }
      
  ngOnInit(){
  
  //   WindowRef.get().FB.ui;
  //   WindowRef.get().fbAsyncInit = function() {
  //   WindowRef.get().FB.init({
  //     appId      : '1823295331318357',
  //     xfbml      : true,
  //     version    : 'v2.10'
  //   });
  //   WindowRef.get().FB.AppEvents.logPageView();
  // };

    // this.FuncionVerificacionFacebook();
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    // console.log(this.identity);
    // console.log(this.token);
    this.mostrarTodo=false;

}
        public FuncionVerificacionFacebook(){
          console.log("Hola amor");
              WindowRef.get().FB.getLoginStatus(function(response) {
              console.log("dentro getLoginStatus");
              console.log(response);
              this.statusChangeCallback(response);
          });
        }

          public statusChangeCallback(response) {
            // The response object is returned with a status field that lets the
            // app know the current login status of the person.
            // Full docs on the response object can be found in the documentation
            // for FB.getLoginStatus().
            if (response.status === 'connected') {
              // Logged into your app and Facebook.
              console.log("El estado de mi funcion es conectada papu");
            } else {
              // The person is not logged into your app or we are unable to tell.
              document.getElementById('status').innerHTML = 'Please log ' +
                'into this app.';
            }
  }

      public loginFacebook(){
          WindowRef.get().FB.login((response)=>{

              if (response.authResponse) {
                console.log(response);
                this.authenticacion=response;
                WindowRef.get().FB.api('/me?fields=id,name,first_name,last_name,devices,locale,location,education,birthday,age_range,email,context,favorite_athletes,favorite_teams,gender,hometown,about', (response)=> {
                console.log(response);
                this.datosBasicos=response;
                this._userService.signupFacebook(this.authenticacion,this.datosBasicos,true).subscribe(response=>{
                  console.log("aca viene el servicio de consumir el servicio");
                  console.log(response);
                  console.log("Como actuo si es")
                          // let userFace=response.userFacebook;
                          let identity=response.userFacebook;
                          this.identity=identity;
                          if(response.token){
                          localStorage.setItem('identity',JSON.stringify(identity));
                            //si recibo un token signfica que se ha registrado correctamente antes y ahora se esta logeando normalmente
                            console.log("Entra el token");
                          let token=response.token;
                          this.token=token;
                          console.log(token);
                          if(!this.token){
                            alert("El tokem no se ha generado");
                          }else{
                            // Crear un elemento en el localstorage para tener un token disponible 
                            localStorage.setItem('token',token);
                            this._router.navigate(['']);
                          }




                          }else{
                            //si no recibe probablemente es la primera vez 
                              if(!identity._id){
                                alert("Error al registrarse");
                                this.errorMessageRegisterFacebook="Error al registrarse";
                              }else{
                                console.log("Ya se registro bien el usuario");
                                //ahora sucedera lo que pasa cuando haces login
                                // this._userService.signupFacebook(this.authenticacion,this.datosBasicos,true).subscribe(res=>{
                                  
                                // },error=>{

                                // });
                            
                              }


                          }





                },error=>{
                  console.log("aca viene un error");
                  console.log(error);

                });
                console.log("Aca viene algo bien chido");

                });
              } else {
              console.log('User cancelled login or did not fully authorize.');
              }
          }, {scope: 'email,user_about_me,user_likes,public_profile,user_birthday,user_education_history,user_location,user_tagged_places'});

      }


  public onSubmit(){
    console.log("Holi Boli");
    console.log(this.user.email);
    console.log(this.user.password);

    this.elementos=document.getElementById("formulario-registro").getElementsByClassName("input-group");
  if(!this.validarInputLoginNormal()){
      console.log("Falta validar los inputs")
    }else{
      console.log("Envia correctamente papu");

    this._userService.signup(this.user).subscribe(
      response=>{
        let identity=response.user;
        this.identity=identity;
        if(!this.identity._id){
          alert("El usuario no esta correctamente identificado");
        }else{
          //Crear elemento en el localstorage para tener al usuario sesion
          localStorage.setItem('identity',JSON.stringify(identity));
          //Conseguir el token para enviarselo a cada peticion http}
//ACA EMPIEZA
                this._userService.signup(this.user,'true').subscribe(
                    response=>{
                      let token=response.token;
                      this.token=token;
                      console.log(token);
                      if(!this.token){
                        alert("El tokem no se ha generado");
                      }else{
                        // Crear un elemento en el localstorage para tener un token disponible 
                        localStorage.setItem('token',token);
                        console.log("El token se ha generado y eres el papu d elos papus");
                        console.log(token);
                        console.log(identity);
                        this._router.navigate(['/mis-test-oficial']);
                      }

                    },error=>{
                      var errorMessage=<any>error;
                      if(errorMessage!=null){
                        var body=JSON.parse(error._body);
                        this.errorMessage=body.message;
                        console.log(error);
                      }
                    }
                  );



//ACA TERMINA

        }

      },error=>{
        var errorMessage=<any>error;
        if(errorMessage!=null){
          var body=JSON.parse(error._body);
          this.errorMessage=body.message;
          console.log(error);
        }
      }
    );
    }
  }
    validarInputLoginNormal(){
    for(var i=0;i<this.elementos.length;i++){
      if(this.elementos[i].children[0].type=="email" || this.elementos[i].children[0].type=="password"){
        if(this.elementos[i].children[0].value==""){
          console.log("El campo "+ this.elementos[i].children[0].name+ " esta completo");
          this.elementos[i].children[0].className=this.elementos[i].children[0].className + ' error';
          return false;
        }else{
          this.elementos[i].children[0].className=this.elementos[i].children[0].className.replace("error", "");
        }

      }

    }
    // if(this.elementos[2].children[0].value!==this.elementos[3].children[0].value){
    //   this.elementos[2].children[0].value="";
    //   this.elementos[3].children[0].value="";
    //   this.elementos[2].children[0].className=this.elementos[2].children[0].className + ' error';
    //   this.elementos[3].children[0].className=this.elementos[3].children[0].className + ' error';
    // }else{
    //   this.elementos[2].children[0].className=this.elementos[2].children[0].className.replace("error", "");
    //   this.elementos[3].children[0].className=this.elementos[3].children[0].className.replace("error", "");
    
    // }
    return true;
  }




    // onButtonClick() {
    //     this.titleKendo = 'Hello from Kendo UI!';
    // }

    logout(){
      localStorage.removeItem('identity');
      localStorage.removeItem('token');
      localStorage.clear();
      this._router.navigate(['/']);
      this.identity=null;
      this.token=null; 
      this.user=new User('','','','','','ROLE_USER','');

    }

    onSubmitRegister(){
    this._userService.register(this.user_register).subscribe(
      response=>{
        let user=response.user;
        this.user_register=user;
        if(!user._id){
          alert("Error al registrarse");
          this.errorMessageRegister="Error al registrarse";
        }else{
          console.log("Entro bien a esta parte");
          this.errorMessageRegister="Registro se ha realizado correctamente, procede a logearte con"+ this.user_register.email;
          this.user_register=new User('','','','','','ROLE_USER','');
      }
      },
      error=>{
          var errorMessageRegister=<any>error;
        if(errorMessageRegister!=null){
          var body=JSON.parse(error._body);
          this.errorMessageRegister=body.message;
        }
      }


    );

  }



  //ACA VA LO DE LOS FORMULARIOS
  // public formulario=document;
  // public elementos=formulario.elements;

 
  enviar(){
this.elementos=document.getElementById("formulario-registro").getElementsByClassName("input-group");
  console.log(this.elementos);  
  if(!this.validarInputs()){
      console.log("Falta vakudar kis inputs")
    }else if(!this.validarRadios()){
      console.log("Falta validar los radio")
    }else if(!this.validarCheckbox()){
      console.log("Falta validar los checkbox")    
    }else{
      console.log("Envia correctamente");
    }

  }


  validarInputs(){
    for(var i=0;i<this.elementos.length;i++){
      if(this.elementos[i].children[0].type=="text" || this.elementos[i].children[0].type=="email" || this.elementos[i].children[0].type=="password"){
        if(this.elementos[i].children[0].value==""){
          console.log("El campo "+ this.elementos[i].children[0].name+ " esta completo");
          this.elementos[i].children[0].className=this.elementos[i].children[0].className + ' error';
          return false;
        }else{
          this.elementos[i].children[0].className=this.elementos[i].children[0].className.replace("error", "");
        }

      }

    }
    if(this.elementos[2].children[0].value!==this.elementos[3].children[0].value){
      this.elementos[2].children[0].value="";
      this.elementos[3].children[0].value="";
      this.elementos[2].children[0].className=this.elementos[2].children[0].className + ' error';
      this.elementos[3].children[0].className=this.elementos[3].children[0].className + ' error';
    }else{
      this.elementos[2].children[0].className=this.elementos[2].children[0].className.replace("error", "");
      this.elementos[3].children[0].className=this.elementos[3].children[0].className.replace("error", "");
    
    }
    return true;
  }


  validarRadios(){
    this.opciones=document.getElementsByName('sexo');
    var resultado=false;
    
    for(var i=0;i<this.opciones.length;i++){
      if(this.opciones[i].checked){
        resultado=true;
        break;
      }
    }
    if(resultado==false){
      this.opciones[0].parentElement.className=this.opciones[0].parentElement.className+ ' error';
      return false;
    }else{
      this.opciones[i].parentElement.className=this.opciones[i].parentElement.className.replace(" error","");
      return true;
    }
  }
  validarCheckbox(){
    this.terminos=document.getElementsByName('terminos');
    var resultado=false;
    
    for(var i=0;i<this.terminos.length;i++){
      if(this.terminos[i].checked){
        resultado=true;
        break;
      }
    }
    if(resultado==false){
      this.terminos[0].parentElement.className=this.terminos[0].parentElement.className+ ' error';
      console.log("Los terminos y condiciones no han sido aceptados");
      return false;
    }else{
      this.terminos[i].parentElement.className=this.terminos[i].parentElement.className.replace(" error","");
      return true;
    }
  }
  focusInput(event){
    console.log(event);
    event.srcElement.parentElement.children[1].className="label active";
    console.log("aca viene algo bien chido");
    console.log(event.srcElement.parentElement.children[0].className);
    event.srcElement.parentElement.children[0].className=event.srcElement.parentElement.children[0].className.replace("error","")

  }
  blurInput(event){
    console.log(event);
    if(event.srcElement.value<=0){
    event.srcElement.parentElement.children[1].className="label";
    event.srcElement.parentElement.children[0].className=event.srcElement.parentElement.children[0].className + " error";
    }

  }

  siguiente(){
    document.getElementById("tata").className="contenedor_tarjeta movimientoVerticalArriba";
    document.getElementById("wawa").className="contenedor_tarjeta movimientoVerticalArriba";
  }
  atras(){
    document.getElementById("tata").classList.remove("movimientoVerticalArriba");
    document.getElementById("wawa").classList.remove("movimientoVerticalArriba");
    
  }
  loguearte(){
    document.getElementById("caraWawa").className="mueveteHorizontal";
    document.getElementById("wawaLogeate").className="mueveteHorizontal";
  }
  loguearteFacebook(){
    console.log("aca viene el logueo con facebook");
  }
  cambiarSideBar(){
    if(this.ocultar){
    document.getElementById("sidebar").classList.remove("mostrarHorizontal");
    document.getElementById("bars").classList.remove("voltearBar");
    
    this.ocultar=false;
    }else{
    console.log("Holi BOli");
    document.getElementById("sidebar").className=document.getElementById("sidebar").className + " mostrarHorizontal";
    document.getElementById("bars").className=document.getElementById("bars").className+" voltearBar";
    this.ocultar=true;
    }
   
  }

}
