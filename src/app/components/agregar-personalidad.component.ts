import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import{PreguntaService} from '../services/pregunta.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import{PreguntaTestGeneral,Alternativas,SkillBasico} from '../models/preguntaTestGeneral';


//naturales
import { Product } from '../models/model';
@Component({
    selector:'editar-test',
    templateUrl:'../views/agregar-personalidad.html',
    providers:[UserService,PreguntaService]
})
export class AgregarPersonalidadComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public pregunta:PreguntaTestGeneral;
        public preguntaPrueb:PreguntaTestGeneral;
        public identity;
        public token;
        public url:string;
        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _preguntaService:PreguntaService
        ){
            this.titulo='Crear Personalidad';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
        }
                public trackByIndex(index: number, item) {
                return index;
                }
      
        ngOnInit(){
            console.log('Agregue tus personalidades papu cargan3do');
        }    
}