import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import Chart from 'chart.js';
import vis from 'vis';
import {Router,ActivatedRoute,Params} from '@angular/router';


@Component({
    selector:'libreria-baby',
    templateUrl:'../views/usando.html',
    providers:[]
})
export class UsandoLibreriaComponent implements OnInit{
        public titulo:string;
        constructor(
                    private _router:Router
        ){}
      
        ngOnInit(){

            console.log('usando libreria baby'); 
            var ctx1 = document.getElementById("myChart");
            var myChart1 = new Chart(ctx1, {
    type: 'bar',
    data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
}); 
// console.log(myChart); 
            var ctx = document.getElementById("myChart2");
            var myChart1 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }



});


//Mi tercer grafico INICIO
        var ctx4 = document.getElementById("myChart3");
        
        var myChart4 = new Chart(ctx4,
            {
                            type: 'radar',
                            data: {
                                labels: [["Eating", "Dinner"], ["Drinking", "Water"], "Sleeping", ["Designing", "Graphics"], "Coding", "Cycling", "Running"],
                                datasets: [{
                                    label: "My First dataset",
                                    backgroundColor:'#ad85e4',
                                    borderColor:'#97c2fc',
                                    pointBackgroundColor:'#7be141',
                                    data: [5,2,5,10,1,7,8]
                                    
                                },]
                            },
                            options: {
                                legend: {
                                    position: 'top',
                                },
                                title: {
                                    display: true,
                                    text: 'Chart.js Radar Chart'
                                },
                                scale: {
                                  
                                ticks: {
                                    beginAtZero: true
                                }
                                }
                            }
                        }
                    );





//Mi tercer grafico FIN

    this.visualizarGrafos();

        }
    visualizarGrafos(){
    var color = 'gray';
    var len = undefined;
    
    var carrera={
        nombreCarrera:"Ing.Sistemas",
        Especializacion:[
            {nombreEspecializacion:"Formacion de Ingenieria",clases:[{titulo:"Ing.Sistemas"},{titulo:"IS101"},{titulo:"IS102"},{titulo:"IS103"},{titulo:"IS104"},{titulo:"IS105"}]},
            {nombreEspecializacion:"Desarrollador",clases:[{titulo:"ISU101"},{titulo:"ISU102"}]},
            {nombreEspecializacion:"Arquitecto de datos",clases:[{titulo:"ISA101"},{titulo:"ISA102"}]},
            {nombreEspecializacion:"Data Science",clases:[{titulo:"ISD101"},{titulo:"ISD102"}]},
            {nombreEspecializacion:"DEVOPS",clases:[{titulo:"ISV101"},{titulo:"ISV102"}]},
            {nombreEspecializacion:"Redes y Seguridad Imformatica",clases:[{titulo:"ISAR01"},{titulo:"ISR102"}]},
            {nombreEspecializacion:"Gestion y planeación",clases:[{titulo:"ISG101"},{titulo:"ISG102"}]},
        ]
    };

    var carreraLongitud=carrera.Especializacion.length;
    var dataPrimera={id: 2, label: "IS102", group: 0};

    var nodes=[];
    var edges=[];
    var cantidad=0;
    var numClases;
    var cantidadRelaciones=0;
    var puntoQuiebre =carrera.Especializacion[0].clases.length-1;


    for (var i = 0; i <carreraLongitud; i++) {
        numClases=carrera.Especializacion[i].clases.length;
        cantidadRelaciones=cantidadRelaciones+carrera.Especializacion[i].clases.length-1;
        for (var j = 0; j <numClases; j++){
        nodes.push({
            id:cantidad,
            label:carrera.Especializacion[i].clases[j].titulo,
            group:i,
            nombreEspecializacion:carrera.Especializacion[i].nombreEspecializacion
        });
        // console.log(nodes);
        //Ahora los nodos to nodos
        cantidad++;
        }
    }

    cantidadRelaciones=cantidadRelaciones+ carreraLongitud-1;

    var suma=carrera.Especializacion[0].clases.length-1;
    var w=0;
    var nodoQuiebre=carrera.Especializacion[0].clases.length-1;
    for(var i = 0; i <cantidadRelaciones; i++){
        if(i==suma){
        edges.push({
            from:nodoQuiebre,
            to:suma+1
        });
            w++;
            suma=suma+carrera.Especializacion[w].clases.length;
        }else{
        edges.push({
            from:i,
            to:i+1
        });

        }

    }


    var container = document.getElementById('mynetwork');
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {
        nodes: {
            shape: 'dot',
            size: 30,
            font: {
                size: 32,
                color: '#1b1818'
            },
            borderWidth: 2
        },
        edges: {
            width: 2
        }
    };
    var network = new vis.Network(container, data, options);
    function obtenerClase(x:number):String{
        return "Holi bOLIT"+ x;

    }
    function getFields(input, field,valorSolicitado) {
    var output = [];
        for (var i=0; i < input.length ; ++i){
        if(input[i][field]==valorSolicitado){
            output.push(input[i]);	
            }
        }
    return output;
    }

    network.on("click", (params)=> {
        params.event = "[original event]";
        document.getElementById('eventSpan').innerHTML = '<h2>Click event:</h2>' + JSON.stringify(params, null, 4);
        // var cositaImprimir=obtenerClase(this.getNodeAt(params.pointer.DOM));
        // console.log(nodes);
        console.log(this);
        var cositaImprimir=network.getNodeAt(params.pointer.DOM);
        if(Number.isInteger(cositaImprimir)){
        var result = getFields(nodes, "id",cositaImprimir);
        console.log("Aca viene mi resultado");
        console.log(result[0].id);
        console.log(result[0].label);
        console.log(result[0].group);
        console.log(result[0].nombreEspecializacion);

        }
 
        
        // console.log('click event, getNodeAt returns: ' + this.getNodeAt(params.pointer.DOM));
    });


    network.on("doubleClick", (params)=> {
        params.event = "[original event]";
        document.getElementById('eventSpan').innerHTML = '<h2>Click event:</h2>' + JSON.stringify(params, null, 4);
        var cositaImprimir=network.getNodeAt(params.pointer.DOM);
        if(Number.isInteger(cositaImprimir)){
        var result = getFields(nodes, "id",cositaImprimir);
        console.log("Aca viene mi resultado");
        console.log(result[0].id);
        console.log(result[0].label);
        console.log(result[0].group);
        console.log(result[0].nombreEspecializacion);
        this._router.navigate(['/claseExplorar']);

        }
 
        
        // console.log('click event, getNodeAt returns: ' + this.getNodeAt(params.pointer.DOM));
    });







    }

}