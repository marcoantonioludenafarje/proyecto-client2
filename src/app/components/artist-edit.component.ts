import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import {ArtistService} from '../services/artist.service';
import {UploadService} from '../services/upload.service';


@Component({
    selector:'artist-add',
    templateUrl:'../views/artist-add.html',
    providers:[UserService,ArtistService,UploadService]
})
export class ArtistEditComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public identity;
        public token;
        public url:string;
        public alertMessage;
        public is_edit;


        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _artistService:ArtistService,
            private _uploadService:UploadService
        ){
            this.titulo='Editar Artista';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
            this.artist=new Artist('','','');
            this.is_edit=true;
        }
      
        ngOnInit(){
 
            console.log('Artist-Edit.component.ts cargado');
            // console.log(this._artistService.addArtist());
            //Conseguir el listado de artistas
            // Llamar al metodo del api para sacar un artsta en base a su id getArtist
            this.getArtist();
        }
        getArtist(){
            this._route.params.forEach((params:Params)=>{
                let id=params['id'];
                this._artistService.getArtist(this.token,id).subscribe(
                    response=>{
                        console.log(response);
                        if(!response.Artist){
                            this._router.navigate(['/']);
                            console.log(response);
                        }else{
                            this.artist=response.Artist;
                            console.log(this.artist);
                        }
                    },
                    error=>{
                    var errorMessage=<any>error;
                    if(errorMessage!=null){
                        var body=JSON.parse(error._body);
                        // this.errorMessage=body.message;
                        console.log(error);
                    }



                    });
            });
        }

    onSubmit(){
            console.log(this.artist);
        this._route.params.forEach((params:Params)=>{
            let id=params['id']; 
            // De esta forma obtenemos el id de la ruta]
            this._artistService.editArtist(this.token,id,this.artist).subscribe(
            response=>{
                console.log(response);
                if(!response.artist){
                    alert("Error en el servidor");
                    this.alertMessage="Error en el servidor";
                }else{
                    // this.artist=response.artist;
                    // this._router.navigate(['/editar-artista'],response.artist._id);
                    this.alertMessage="El artista se ha actualizado correctamente";

                    //subir la imagen del artista
                    console.log(this.filesToUpload);
                    this._uploadService.makeFileRequest(this.url+'upload-image-artist/'+id,[], this.filesToUpload, this.token, 'image')
                                .then(
                                    (result)=>{
                                        this._router.navigate(['/artistas',1]);
                                    },
                                    (error)=>{
                                        console.log(error);
                                    }

                                );
                }
            },
            error=>{
                    var errorMessage=<any>error;
                    if(errorMessage!=null){
                        var body=JSON.parse(error._body);
                        // this.errorMessage=body.message;
                        console.log(error);
                    }
            });
        });


        }
        public filesToUpload:Array<File>;
        fileChangeEvent(fileInput:any){
            this.filesToUpload=<Array<File>>fileInput.target.files;
            console.log(this.filesToUpload);


        }

}