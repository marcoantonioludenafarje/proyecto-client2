import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import {AlbumService} from '../services/album.service';
import {ArtistService} from '../services/artist.service';
import {UploadService} from '../services/upload.service';
import{Album} from '../models/album';



@Component({
    selector:'album-add',
    templateUrl:'../views/album-add.html',
    providers:[UserService,ArtistService,UploadService,AlbumService]
})
export class AlbumAddComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public album:Album;
        public identity;
        public token;
        public url:string;
        public alertMessage;
        public is_edit;


        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _artistService:ArtistService,
            private _uploadService:UploadService,
            private _albumService:AlbumService
        ){
            this.titulo='Crear nuevo Album';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
            this.album=new Album('','',2017,'','');
            // this.is_edit=true;
        }
      
        ngOnInit(){
 
            console.log('AlbumAdd.component.ts cargado');
            // console.log(this._artistService.addArtist());
            //Conseguir el listado de artistas
            // Llamar al metodo del api para sacar un artsta en base a su id getArtist
     
        }
        onSubmit(){
            this._route.params.forEach((params:Params)=>{
                this.album.artist=params['artista'];
                        console.log(this.album);

                this._albumService.addAlbum(this.token,this.album).subscribe(
                    response=>{
                        console.log(response);
                        if(!response.album){
                            this._router.navigate(['/']);
                             this.alertMessage="Error en el servidor!!";
                            console.log(response);
                        }else{
                            this.alertMessage="El album ha sido creado exitosamente !!";
                            this.album=response.album;
                            // this._router.navigate
                            this._router.navigate(['/editar-album',response.album._id]);
                            console.log(this.album);
                        }
                    },
                    error=>{
                        var errorMessage=<any>error;
                        if(errorMessage!=null){
                            var body=JSON.parse(error._body);
                            this.alertMessage=body.message;
                            // this.errorMessage=body.message;
                            console.log(error);
                        }



                    });
            });
        }
       
}