import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import {ArtistService} from '../services/artist.service';
@Component({
    selector:'artist-add',
    templateUrl:'../views/artist-add.html',
    providers:[UserService,ArtistService]
})
export class ArtistAddComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public identity;
        public token;
        public url:string;
        public alertMessage;


        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _artistService:ArtistService
        ){
            this.titulo='Crear Artista';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
            this.artist=new Artist('','','');
        }
      
        ngOnInit(){
 
            console.log('Artist-list.component.ts cargado');
            // console.log(this._artistService.addArtist());
            //Conseguir el listado de artistas
        }    

        onSubmit(){
            console.log(this.artist);
            this._artistService.addArtist(this.token,this.artist).subscribe(
            response=>{
                console.log(response);
                if(!response.artist){
                    alert("Error en el servidor");
                    this.alertMessage="Error en el servidor";
                }else{
                    this.artist=response.artist;
                    this._router.navigate(['/editar-artista',response.artist._id]);
                    this.alertMessage="El artista se ha creado correctamente";
                }
            },
            error=>{
                    var errorMessage=<any>error;
                    if(errorMessage!=null){
                        var body=JSON.parse(error._body);
                        // this.errorMessage=body.message;
                        console.log(error);
                    }
            })
        }


}