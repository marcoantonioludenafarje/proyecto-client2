import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import {ArtistService} from '../services/artist.service';
import{Album} from '../models/album';
import {AlbumService} from '../services/album.service';


@Component({
    selector:'artist-detail',
    templateUrl:'../views/artist-detail.html',
    providers:[UserService,ArtistService,AlbumService]
})
export class ArtistDetailComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public identity;
        public token;
        public url:string;
        public alertMessage;
        public albums:Album[];
      


        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _artistService:ArtistService,
             private _albumService:AlbumService       
        ){
            this.titulo='Editar Artista';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
        }
      
        ngOnInit(){
 
            console.log('Artist-Edit.component.ts cargado');
            // console.log(this._artistService.addArtist());
            //Conseguir el listado de artistas
            // Llamar al metodo del api para sacar un artsta en base a su id getArtist
            this.getArtist();
        }
        getArtist(){
            this._route.params.forEach((params:Params)=>{
                let id=params['id'];
                this._artistService.getArtist(this.token,id).subscribe(
                    response=>{
                        
                        if(!response.Artist){
                            this._router.navigate(['/']);
                            console.log(response);
                        }else{
                            this.artist=response.Artist;
                            console.log(this.artist);
                            // sacar los albums del artista
                            this._albumService.getAlbums(this.token,response.Artist._id).subscribe(
                                response=>{
                                    this.albums=response.albums;
                                    if(!response.albums){
                                        this.alertMessage='Este artista no tiene albums';
                                    }else{
                                        this.albums=response.albums;
                                        console.log(this.albums);
                                    }

                                },error=>{
                                    var errorMessage=<any>error;
                                    if(errorMessage!=null){
                                        var body=JSON.parse(error._body);
                                        // this.errorMessage=body.message;
                                        console.log(error);
                                    }
                                });
                        }
                    },
                    error=>{
                    var errorMessage=<any>error;
                    if(errorMessage!=null){
                        var body=JSON.parse(error._body);
                        // this.errorMessage=body.message;
                        console.log(error);
                    }



                    });
            });
        }

        public confirmado;
        onDeleteConfirm(id){
            this.confirmado=id;

        }
        onCancelAlbum(){
            this.confirmado=null;
        }
        onDeleteAlbum(id){
            this._albumService.deleteAlbum(this.token,id).subscribe(
                    response=>{
                        console.log("la respuesta ante el responde de el album eleiminado");
                        console.log(response);
                        if(!response.AlbumRemovido){
                            alert('Error en el servidor')
                        }else{
                            this._albumService.getAlbums(this.token,response.AlbumRemovido.artist).subscribe(
                                response=>{
                                    console.log("aca viene la respuesta de la recarga de albums");
                                    console.log(response);
                                    this.albums=response.albums;
                                    if(!response.albums){
                                        this.alertMessage='Este artista no tiene albums';
                                    }else{
                                        this.albums=response.albums;
                                        console.log(this.albums);
                                    }

                                },error=>{
                                    var errorMessage=<any>error;
                                    if(errorMessage!=null){
                                        var body=JSON.parse(error._body);
                                        // this.errorMessage=body.message;
                                        console.log(error);
                                    }
                                });
                        }
                    },
                    error=>{
                        console.log(error);
                        var errorMessage=<any>error;
                        if(errorMessage!=null){
                            var body=JSON.parse(error._body);
                            // this.errorMessage=body.message;
                            console.log(error);
                        }

                    }
            )
        }


}