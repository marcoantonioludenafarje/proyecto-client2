import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import{PreguntaTestGeneral} from '../models/preguntaTestGeneral';
import{PreguntaService} from '../services/pregunta.service';
import{Product} from '../models/model';
import{TestResultado,CheckExplFrec,CheckyDescrip} from '../models/testResultado';

// import{ResultadoTest} from '../models/resultadoTest';
// import {EditarTestComponent} from './editar-test.component';
@Component({
    selector:'mis-test',
    templateUrl:'../views/mis-test.html',
    providers:[UserService,PreguntaService]
})
export class MisTestComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public identity;
        public user:User;
        public token;
        public url:string;
        public alertMessage;
        public preguntas=[];
        public preguntaActiva;
        public testResultado:TestResultado;
        public carreras=["Ingenieria Ambiental","Ingenieria de Sistemas","Ingenieria Industrial"];
        public numPag;
        public mostrarParte=false;
        public circulos=[];
        public mostrarCirculos=false;
        public siguiente="siguiente";
        public messageMostrar:boolean;
        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _preguntaService:PreguntaService
        ){
            this.titulo='Mis test';
            this.identity=this._userService.getIdentity();
            this.user =this.identity;
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
            this.artist=new Artist('','','');
            this.testResultado=new TestResultado();
            // this.testResultado.estaEtapa=new CheckyDescrip();
            // this.testResultado.relPadres=new CheckExplFrec();
            // this.testResultado.estaEtapa.resul="Escoge una opcion";
            // this.testResultado.relPadres.resul="Escoge una opcion";
            // this.testResultado.relPadres.frec="Escoge una opcion"; 
            // this.testResultado.primerInteres="Primera opcion (Obligatorio)";
            // this.testResultado.segundoInteres="Segunda opcion (Opcional)";
            // this.testResultado.resultado=[];
            // this.user.puntajeSkills=[];
            this.numPag=0;
            this.messageMostrar=false;
            // this.testResultado.puntajeSkills=[
            //     {nombreSkill:"realista",puntaje:0},
            //     {nombreSkill:"investigador",puntaje:0},
            //     {nombreSkill:"artista",puntaje:0},
            //     {nombreSkill:"social",puntaje:0},
            //     {nombreSkill:"emprendedor",puntaje:0},
            //     {nombreSkill:"convencional",puntaje:0}
            // ];
        }
      
        ngOnInit(){
 
            // console.log('Mis -test.component.ts cargado');
            // this.getPreguntas();
            // setTimeout(()=>{    //<<<---    using ()=> syntax
            // this.pintarCirculos(this.numPag);
            // },1500);
        }
        // }
        // getPreguntas(){

        //         this._preguntaService.getPreguntasTest(this.token).subscribe(
        //            response=>{
        //                 if(!response.preguntas){
        //                     this._router.navigate(['/']);
        //                 }else{
        //                     this.preguntas=response.preguntas;
        //                     console.log(this.preguntas);
                                        
        //                     for(var i=0;i<=this.preguntas.length;i++){
        //                     this.circulos.push(i);
        //                     }
        //                     this.mostrarCirculos=true;

        //                 }
        //             },
        //             error=>{
        //                 var errorMessage=<any>error;
        //                 if(errorMessage!=null){
        //                     var body=JSON.parse(error._body);
        //                     // this.errorMessage=body.message;
        //                     console.log(error);
        //                 }

        //             }


        //         );
        // }  
        // public trackByIndex(index: number, item) {
        //         return index;
        //         }
        // onSubmit(){
        //     console.log(this.artist);
        // }
        // sumarPapu(){
        //     console.log("HolitasBolitas");
        // }
        // onBlur(){
        //     console.log("En esta accion");
        // }
        // onFocus(){
        //     console.log("Enfocada")
        // }
        // esocogio(alternativa){
        //     console.log(alternativa);
        // }
        // public usuario:User;
        // unicaAlternativa(numAlt,nombreId,elementoReferenciar){

        //     var y:any=document.getElementById(nombreId+numAlt);
        //     var grupo=y.name;
        //     var x:any=document.getElementsByName(grupo);
        //         for ( var i = 0; i < x.length; i++) {
        //             if (x[i].type == "checkbox") {
        //                 x[i].checked = false;
        //             }
        //         }
        //     y.checked=true;
        //     this.testResultado[elementoReferenciar]=numAlt;
        // }
        // verQuePaso(){
        //     console.log(this.testResultado.estaEtapa);
        // }
        // public continuar0=false;
        // public continuarN=false;
        // validarDatosPag(){
        // if(this.numPag==0){
        //     var z:any=document.getElementsByClassName("oblig0");
        //     for(var i=0;i<z.length;i++){
        //         z[i].classList.remove("falta");
        //     }
        //     if( this.testResultado.primerInteres!="Primera opcion (Obligatorio)" && this.testResultado.estaEtapa.resul!="Escoge una opcion" && this.testResultado.tipoEstudiante!=null && this.testResultado.relPadres.resul!="Escoge una opcion" && this.testResultado.relPadres.frec!="Escoge una opcion"){
        //         this.continuarN=true;
        //         this.messageMostrar=false;
        //     }else{
        //         this.continuarN=false;
        //         this.messageMostrar=true;
        //         var z:any=document.getElementsByClassName("oblig0");
        //         console.log(z);
        //         if(this.testResultado.tipoEstudiante==null){
        //         z[0].classList.add("falta");
        //         }
        //         if(this.testResultado.estaEtapa.resul=="Escoge una opcion"){
        //         z[1].classList.add("falta");
        //         }
        //         if(this.testResultado.relPadres.resul=="Escoge una opcion"){
        //         z[2].classList.add("falta");
        //         } 
        //         if(this.testResultado.relPadres.frec=="Escoge una opcion"){
        //         z[3].classList.add("falta");
        //         } 
        //         if(this.testResultado.primerInteres=="Primera opcion (Obligatorio)"){
        //          z[4].classList.add("falta");
        //         } 
        //     }
        // }else{
        //     var w:any=document.getElementById("enunciadoActivo");
        //     w.classList.remove("falta");
        //     console.log(this.testResultado.resultado[this.numPag-1].alternativasMarcadas);

        //     if(this.numPag>0 && this.testResultado.resultado[this.numPag-1].alternativasMarcadas.length>0){
        //         this.continuarN=true;
        //         this.messageMostrar=false;
        //     }else{
        //         this.continuarN=false;
        //         this.messageMostrar=true;
        //         var z:any=document.getElementById("enunciadoActivo");
        //         z.classList.add("falta");
        //     }
        // }
        // }
        // SiguientePaso(direccion){
        //     // validaciones de movimiento    
        //     if(direccion=="+"){
        //         this.validarDatosPag();
        //         if(this.continuarN==true){
        //             this.numPag++;
        //         }
            
        //     }
        //     //carga pasando a la pagina 
        //     if(this.numPag==0){
        //         this.mostrarParte=false;
        //     }else{
        //         if(this.numPag>0 && this.numPag<=this.preguntas.length){
        //             this.mostrarParte=true;
        //             this.preguntaActiva=this.preguntas[this.numPag-1];
        //             console.log("el numero de pagina");
        //             console.log(this.numPag);
        //             if(this.continuarN==true){
        //             this.testResultado.resultado.push({
        //             numPregunta:this.numPag-1,alternativasMarcadas:[]
        //             });
        //             console.log("aca viene algo que se itera en cada siguiente");
        //             console.log(this.testResultado);
        //             }

        //             if(this.numPag==this.preguntas.length){
        //             this.siguiente="Obtener resultados";
        //             }
                    
                    



        //         }else{
        //             console.log("Estas son las preguntas");
        //             console.log(this.preguntas);
        //             console.log("Estos es el testResultado");
        //             console.log(this.testResultado);
        //             for(var i=0;i<this.testResultado.resultado.length;i++){
        //                 //se va a hacer una iteracion para cada pregunta
        //                 for(var j=0;j<this.testResultado.resultado[i].alternativasMarcadas.length;j++){
        //                     //aca me muestra las alternativas marcadas  para esa pregunta en i 
        //                     var process=(elem)=>{
        //                         if(elem._id=this.testResultado.resultado[i].alternativasMarcadas[j]){return elem;}
        //                     }
        //                     var index=this.preguntas[i].alternativas.findIndex(process);
        //                     //Este metodo lo que hara es darme el index de las alternativas marcadas                            
        //                     for(var w=0;w<this.preguntas[i].alternativas[index].valorSkills.length;w++){
        //                     var skillBasico=this.preguntas[i].alternativas[index].valorSkills[w].skillBasico;
        //                     var buscarSkill=(nomb)=>{
        //                         if(nomb.nombreSkill==skillBasico){
        //                             return nomb;
        //                         }
        //                     }
        
        //                     var ind=this.testResultado.puntajeSkills.findIndex(buscarSkill);
        //                     if(this.preguntas[i].alternativas[index].valorSkills[w].puntos==null){
        //                         this.preguntas[i].alternativas[index].valorSkills[w].puntos=0;
        //                     }
        //                     console.log("aca viene lo que busco");
        //                     console.log(ind);
        //                     console.log(i);
        //                     console.log(this.preguntas[i].alternativas[index].valorSkills[w].puntos);
        //                     this.testResultado.puntajeSkills[ind].puntaje=this.testResultado.puntajeSkills[ind].puntaje+this.preguntas[i].alternativas[index].valorSkills[w].puntos;
        //                     console.log(this.testResultado.puntajeSkills[ind].puntaje);
        //                     }
        //                 }
        //             }
        //             // this._router.navigate(['/mis-carreras']);
        //             //Osea son >1 , aca debo reemplazar unicamente la pregunta en el mismo molde
        //             console.log("Veamos que tenemos");
        //             console.log(this.testResultado);
        //             console.log(this.user);
        //             this.user.testResultado=this.testResultado;
        //             console.log(this.user);
        //                       this._userService.updateUser(this.user).subscribe(
        //                         response=>{
        //                         console.log("Aca viene la respuesta del servicio de update");
        //                         console.log(response);
        //                         this.user=response.user;
        //                         if(!response.user){
        //                             this.alertMessage='Tus resultados fueron exitosos';
        //                         }else{

        //                             // this.alertMessage='Todo salio genial';
        //                             this._router.navigate(['/mis-carreras']);
        //                         }

        //                     },error=>{
        //                                 var errorMessage=<any>error;
        //                                 if(errorMessage!=null){
        //                                     var body=JSON.parse(error._body);
        //                                     this.alertMessage=body.message;
        //                                     console.log(error);
        //                                 }


        //                     })






        //         }
        //     }
        //     if(this.numPag<=this.preguntas.length){
        //     this.pintarCirculos(this.numPag);
        //     }

        // }
        // pintarCirculos(numPag){
        // var x:any=document.getElementsByClassName("circulo");

        // for ( var i = 0; i <=numPag; i++) {
        // // x[i].classList.remove("pintamelo");
        // x[i].classList.add("pintamelo");
        
        // }
        // // x[numPag].classList.add("pintamelo");
        // // console.log(x[numPag]);

        // }
        // multipleAlternativa(alternativa){
        //     function buscarAlternativa(alt){if(alt==alternativa._id){return alt ;}}
        //     var resultados=this.testResultado.resultado[this.numPag-1].alternativasMarcadas.find(buscarAlternativa);
        //     if(resultados!=undefined){
        //         //SE ENCUENTRA ACA
        //         var index = this.testResultado.resultado[this.numPag-1].alternativasMarcadas.indexOf(alternativa._id);
        //         console.log("Entra aca");
        //         if (index > -1) {
        //             this.testResultado.resultado[this.numPag-1].alternativasMarcadas.splice(index, 1);
        //         }else{
        //             console.log("A ocurrido un error")
        //         }
        //     }else{
        //         //NO SE ENCONTRABA ACA
        //         this.testResultado.resultado[this.numPag-1].alternativasMarcadas.push(alternativa._id);
        //     }
        // }
        
}