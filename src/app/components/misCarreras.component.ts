import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
@Component({
    selector:'claseCarreraExplorar',
    templateUrl:'../views/misCarreras.html',
    providers:[UserService]
})
export class MisCarrerasComponent implements OnInit{
        public titulo:string;
        // public artist:Artist;
        // public identity;
        // public token;
        // public url:string;
        public carreras=[
            {
                nombre:"Ingenieria de Sistemas",
                descripcion:"Es una carrera Estas personas prefieren conducir o dirigir personas"
            },
            {
                nombre:"Ingenieria Industrial",
                descripcion:"Es una carrera Estas personas prefieren conducir o dirigir personas"
            },
            {
                nombre:"Ingenieria Ambiental",
                descripcion:"Es una carrera Estas personas prefieren conducir o dirigir personas"
            },
            {
                nombre:"Ingenieria Mecatronica",
                descripcion:"Es una carrera Estas personas prefieren conducir o dirigir personas"
            },
            {
                nombre:"Economista",
                descripcion:"Es una carrera Estas personas prefieren conducir o dirigir personas"
            },
            {
                nombre:"Ingeniero Civil",
                descripcion:"Es una carrera Estas personas prefieren conducir o dirigir personas"
            },
            {
                nombre:"Ingenieria de Minas",
                descripcion:"Es una carrera Estas personas prefieren conducir o dirigir personas"
            }
        ]


        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            // private _userService:UserService
        ){
            this.titulo='Mis Carrera';
            // this.identity=this._userService.getIdentity();
            // this.token=this._userService.getToken();
            // this.url=GLOBAL.url;
            // this.artist=new Artist('','','');
        }
      
        ngOnInit(){
 
            console.log('Clase Carrera.component.ts cargado');
            //Conseguir el listado de artistas
        }    

        onSubmit(){
       
        }


}