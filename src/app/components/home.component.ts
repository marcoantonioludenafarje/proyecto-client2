import{Component , OnInit} from '@angular/core';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import{UserService} from '../services/user.service';
import {GLOBAL} from '../services/global';
@Component({
    selector:'artist-list',
    templateUrl:'../views/home.html',
    providers:[UserService]
})
export class HomeComponent implements OnInit{
        public titulo:string;
        public user:User;
        public identity;
        public token;
        public alertMessage;
        public url:string;
        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService
        ){
        // localStorage
        this.identity=this._userService.getIdentity();
        // Con esto ya obtengo los datos de mi usuario
        this.token=this._userService.getToken();
        // Ahora se lo asigno al modelo del formulario con lo cual ya se actualiza el formulario
        this.user =this.identity;
        this.url=GLOBAL.url;
        }
      
        ngOnInit(){
 
            console.log('Home component esta cargado');
            if(this.user.name!=null){
            this.titulo='BIENVENIDO '+this.user.name+ '!!' ;
            }
            //Conseguir el listado de artistas
        }    




}