import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import vis from 'vis';
@Component({
    selector:'claseCarreraExplorar',
    templateUrl:'../views/miCarrera-detail.html',
    providers:[UserService]
})
export class MisCarrerasDetailComponent implements OnInit{
        public titulo:string;
        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            // private _userService:UserService
        ){
            this.titulo='Ingenieria de Sistemas';
        }
      
        ngOnInit(){
 
            console.log('Clase Carrera.component.ts cargado');
            //Conseguir el listado de artistas
                this.visualizarGrafos();
        }    

        onSubmit(){
       
        }
    visualizarGrafos(){
    var color = 'gray';
    var len = undefined;
    
    var carrera={
        nombreCarrera:"Ing.Sistemas",
        Especializacion:[
            {nombreEspecializacion:"Formacion de Ingenieria",clases:[{titulo:"Ing.Sistemas"},{titulo:"IS101"},{titulo:"IS102"},{titulo:"IS103"},{titulo:"IS104"},{titulo:"IS105"}]},
            {nombreEspecializacion:"Desarrollador",clases:[{titulo:"ISU101"},{titulo:"ISU102"}]},
            {nombreEspecializacion:"Arquitecto de datos",clases:[{titulo:"ISA101"},{titulo:"ISA102"}]},
            {nombreEspecializacion:"Data Science",clases:[{titulo:"ISD101"},{titulo:"ISD102"}]},
            {nombreEspecializacion:"DEVOPS",clases:[{titulo:"ISV101"},{titulo:"ISV102"}]},
            {nombreEspecializacion:"Redes y Seguridad Imformatica",clases:[{titulo:"ISAR01"},{titulo:"ISR102"}]},
            {nombreEspecializacion:"Gestion y planeación",clases:[{titulo:"ISG101"},{titulo:"ISG102"}]},
        ]
    };

    var carreraLongitud=carrera.Especializacion.length;
    var dataPrimera={id: 2, label: "IS102", group: 0};

    var nodes=[];
    var edges=[];
    var cantidad=0;
    var numClases;
    var cantidadRelaciones=0;
    var puntoQuiebre =carrera.Especializacion[0].clases.length-1;


    for (var i = 0; i <carreraLongitud; i++) {
        numClases=carrera.Especializacion[i].clases.length;
        cantidadRelaciones=cantidadRelaciones+carrera.Especializacion[i].clases.length-1;
        for (var j = 0; j <numClases; j++){
        nodes.push({
            id:cantidad,
            label:carrera.Especializacion[i].clases[j].titulo,
            group:i,
            nombreEspecializacion:carrera.Especializacion[i].nombreEspecializacion
        });
        // console.log(nodes);
        //Ahora los nodos to nodos
        cantidad++;
        }
    }

    cantidadRelaciones=cantidadRelaciones+ carreraLongitud-1;

    var suma=carrera.Especializacion[0].clases.length-1;
    var w=0;
    var nodoQuiebre=carrera.Especializacion[0].clases.length-1;
    for(var i = 0; i <cantidadRelaciones; i++){
        if(i==suma){
        edges.push({
            from:nodoQuiebre,
            to:suma+1
        });
            w++;
            suma=suma+carrera.Especializacion[w].clases.length;
        }else{
        edges.push({
            from:i,
            to:i+1
        });

        }

    }


    var container = document.getElementById('mynetwork');
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {
        nodes: {
            shape: 'dot',
            size: 15,
            font: {
                size: 12,
                color: '#1b1818'
            },
            borderWidth: 1
        },
        edges: {
            width: 1
        }
    };
    var network = new vis.Network(container, data, options);
    function obtenerClase(x:number):String{
        return "Holi bOLIT"+ x;

    }
    function getFields(input, field,valorSolicitado) {
    var output = [];
        for (var i=0; i < input.length ; ++i){
        if(input[i][field]==valorSolicitado){
            output.push(input[i]);	
            }
        }
    return output;
    }

    network.on("click", (params)=> {
        params.event = "[original event]";
        document.getElementById('eventSpan').innerHTML = '<h2>Click event:</h2>' + JSON.stringify(params, null, 4);
        // var cositaImprimir=obtenerClase(this.getNodeAt(params.pointer.DOM));
        // console.log(nodes);
        console.log(this);
        var cositaImprimir=network.getNodeAt(params.pointer.DOM);
        if(Number.isInteger(cositaImprimir)){
        var result = getFields(nodes, "id",cositaImprimir);
        console.log("Aca viene mi resultado");
        console.log(result[0].id);
        console.log(result[0].label);
        console.log(result[0].group);
        console.log(result[0].nombreEspecializacion);

        }
 
        
        // console.log('click event, getNodeAt returns: ' + this.getNodeAt(params.pointer.DOM));
    });


    network.on("doubleClick", (params)=> {
        params.event = "[original event]";
        document.getElementById('eventSpan').innerHTML = '<h2>Click event:</h2>' + JSON.stringify(params, null, 4);
        var cositaImprimir=network.getNodeAt(params.pointer.DOM);
        if(Number.isInteger(cositaImprimir)){
        var result = getFields(nodes, "id",cositaImprimir);
        console.log("Aca viene mi resultado");
        console.log(result[0].id);
        console.log(result[0].label);
        console.log(result[0].group);
        console.log(result[0].nombreEspecializacion);
        this._router.navigate(['/clase-explorar/4']);

        }
 
        
        // console.log('click event, getNodeAt returns: ' + this.getNodeAt(params.pointer.DOM));
    });







    }

}