import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import{PreguntaService} from '../services/pregunta.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import{PreguntaTestGeneral,Alternativas,SkillBasico} from '../models/preguntaTestGeneral';
import Chart from 'chart.js';

//naturales
import { Product } from '../models/model';
@Component({
    selector:'editar-test',
    templateUrl:'../views/administracion-usuarios.html',
    providers:[UserService,PreguntaService]
})
export class AdministracionUsuariosComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public pregunta:PreguntaTestGeneral;
        public preguntaPrueb:PreguntaTestGeneral;
        public identity;
        public token;
        public url:string;
        public porcentajesSalon;
        public usuarios:any;
        public nuevoUsuario:boolean;
        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _preguntaService:PreguntaService
        ){
            this.titulo='Panel de control de Usuarios';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
            this.nuevoUsuario=false;
        }
                public trackByIndex(index: number, item) {
                return index;
                }
      
        ngOnInit(){
            console.log('Panel de control');
            this.getUsers();

        }
        getUsers(){
             this._userService.getUsers().subscribe(
                   response=>{
                        console.log("Aca viene nuestra respuesta");
                        console.log(response);
                        if(!response){
                            alert("No recibio el resultado esperado");
                        }else{
                            console.log("Supuestamente si hay respuesta");
                            this.usuarios=response;
                            console.log(this.usuarios);
                        }
                    },
                    error=>{
                        var errorMessage=<any>error;
                        if(errorMessage!=null){
                            // var body=JSON.parse(error._body);
                            // this.errorMessage=body.message;
                            console.log(error);
                        }

                    }
                )   
        }
        activarNuevoUsuario(){
            this.nuevoUsuario=true;
        }
        
}