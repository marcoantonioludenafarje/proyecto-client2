import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import{PreguntaService} from '../services/pregunta.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import{PreguntaTestGeneral,Alternativas,SkillBasico} from '../models/preguntaTestGeneral';
import Chart from 'chart.js';

//naturales
import { Product } from '../models/model';
@Component({
    selector:'editar-test',
    templateUrl:'../views/panel-control-alumnos.html',
    providers:[UserService,PreguntaService]
})
export class PanelControlAlumnosComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public pregunta:PreguntaTestGeneral;
        public preguntaPrueb:PreguntaTestGeneral;
        public identity;
        public token;
        public url:string;
        public porcentajesSalon;
        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _preguntaService:PreguntaService
        ){
            this.titulo='Panel de control';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
        }
                public trackByIndex(index: number, item) {
                return index;
                }
      
        ngOnInit(){
            console.log('Panel de control');
            this.porcentajesSalon=[
                {num:0,puntaje:10},
                {num:1,puntaje:20},
                {num:2,puntaje:30},
                {num:3,puntaje:10},
                {num:4,puntaje:10},
                {num:5,puntaje:20},
            ];
            this.hacerGraficaRadar();
        }
        
        
            hacerGraficaRadar(){
        var ctx4 = document.getElementById("bar-chart");
        
new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [2478,5267,734,784,433]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Predicted world population (millions) in 2050'
      }
    }
});
new Chart(document.getElementById("doughnut-chart"), {
    type: 'doughnut',
    data: {
      labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [2478,5267,734,784,433]
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Predicted world population (millions) in 2050'
      }
    }
});
                    
        // document.getElementById("myChart3").style.height="200px";
        // document.getElementById("myChart3").style.width="200px";
        }
}