import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import {AlbumService} from '../services/album.service';
import {ArtistService} from '../services/artist.service';
import {UploadService} from '../services/upload.service';
import{Album} from '../models/album';

@Component({
    selector:'album-edit',
    templateUrl:'../views/album-add.html',
    providers:[UserService,ArtistService,UploadService,AlbumService]
})
export class AlbumEditComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public album:Album;
        public identity;
        public token;
        public url:string;
        public alertMessage;
        public is_edit;


        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _artistService:ArtistService,
            private _uploadService:UploadService,
            private _albumService:AlbumService
        ){
            this.titulo='Editar album';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
            // this.artist=new Artist('','','');
            this.is_edit=true;
            this.album=new Album('','',2017,'','');
        }
      
        ngOnInit(){
 
            console.log('Album-Edit.component.ts cargado');
            // console.log(this._artistService.addArtist());
            //Conseguir el listado de artistas
            // Llamar al metodo del api para sacar un artsta en base a su id getArtist
            this.getAlbum();
        }
        getAlbum(){
            this._route.params.forEach((params:Params)=>{
                let id=params['id'];
                this._albumService.getAlbum(this.token,id).subscribe(
                    response=>{
                        // console.log(response);
                        if(!response.album){
                            this._router.navigate(['/']);
                            console.log(response);
                        }else{
                            this.album=response.album;
                        
                        }
                    },
                    error=>{
                    var errorMessage=<any>error;
                    if(errorMessage!=null){
                        var body=JSON.parse(error._body);
                        // this.errorMessage=body.message;
                        console.log(error);
                    }



                    });
            });
        }

    onSubmit(){
        console.log(this.album);

            this._route.params.forEach((params:Params)=>{
            let id=params['id']; 
            // De esta forma obtenemos el id de la ruta]
            this._albumService.editAlbum(this.token,id,this.album).subscribe(
            response=>{
                if(!response.album){
                    this.alertMessage="Error en el servidor";
                }else{
                    this.alertMessage="El album se ha actualizado correctamente";
                    if(!this.filesToUpload){
                        // console.log(this.fil)
                        this._router.navigate(['/artista',response.album.artist]);
                        console.log(this.album);
                    }else{
                    console.log("Lo que me trae el resposne");
                    console.log(response);
                    this._uploadService.makeFileRequest(this.url+'upload-image-album/'+id,[], this.filesToUpload, this.token, 'image')
                                .then(
                                    (result)=>{
                                        this._router.navigate(['/artista',response.album.artist]);
                                        console.log("Habilitando el result");
                                        
                                        
                                    },
                                    (error)=>{
                                        console.log(error);
                                    }

                                );
                    }
                }
            },
            error=>{
                    var errorMessage=<any>error;
                    if(errorMessage!=null){
                        var body=JSON.parse(error._body);
                        // this.errorMessage=body.message;
                        console.log(error);
                    }
            });
        });


        }
        public filesToUpload:Array<File>;
        fileChangeEvent(fileInput:any){
            this.filesToUpload=<Array<File>>fileInput.target.files;
            console.log(this.filesToUpload);


        }

}