import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import{PreguntaService} from '../services/pregunta.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import{PreguntaTestGeneral} from '../models/preguntaTestGeneral';


//naturales
import { Product } from '../models/model';
@Component({
    selector:'configurar-test',
    templateUrl:'../views/configurar-test.html',
    providers:[UserService,PreguntaService]
})
export class ConfigurarTestComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public pregunta:PreguntaTestGeneral;
        public identity;
        public token;
        public url:string;
        public skills=["realista","investigador","artista","social","emprendedor","convencional"];
        public  names: string[];
        public numAlternativa:number;
        public pruebitas=[{nombre:"Marco Ludeña",apellido:"Farje"},{nombre:"Juan",apellido:"Camacho"},{nombre:"Cosito",apellido:"Farjesino"}];
        public next_page;
        public prev_page;
        public preguntas:PreguntaTestGeneral[];
        public tiposPreguntas=["Una sola es correcta","Todas las alternativas son validas"];
        public alertMessage;
        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _preguntaService:PreguntaService
        ){
            this.titulo='Configuraciones Test General';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
            this.next_page=1;
            this.prev_page=1;
        }
        public trackByIndex(index: number, item) {
        return index;
        }
      
        ngOnInit(){
 
            console.log('Configurar-test.component.ts cargado');
            this.getPreguntas();

        }
        getPreguntas(){
            this._route.params.forEach((params:Params)=>{
                let page=+params['page'];
                if(!page){
                    page=1;
                }else{
                    this.next_page=page+1;
                    this.prev_page=page-1;
                    if(this.prev_page==0){
                        this.prev_page=1;
                    }
                }
                this._preguntaService.getPreguntas(this.token,page).subscribe(
                   response=>{
                        console.log(response);
                        if(!response.preguntas){
                            this._router.navigate(['/']);
                            console.log(response);
                        }else{
                            this.preguntas=response.preguntas;
                            console.log(this.preguntas);
                        }
                    },
                    error=>{
                        var errorMessage=<any>error;
                        if(errorMessage!=null){
                            var body=JSON.parse(error._body);
                            // this.errorMessage=body.message;
                            console.log(error);
                        }

                    }


                )

            });
        }  

        onSubmit(){
            console.log("Respuesta del ONSUBMIT")
            console.log(this.preguntas);
        
        }

        eliminarPregunta(numAlt:number){
            let id=this.preguntas[numAlt]._id;
            this._preguntaService.deletePregunta(this.token,id).subscribe(
                    response=>{
                        console.log(response);
                        if(!response.preguntaRemovida){
                            alert('Error en el servidor')
                        }else{
                            this.getPreguntas();
                        }
                    },
                    error=>{
                        console.log(error);
                        var errorMessage=<any>error;
                        if(errorMessage!=null){
                            var body=JSON.parse(error._body);
                            // this.errorMessage=body.message;
                            console.log(error);
                        }

                    }
            )

        }
        editarPregunta(numAlt:number){
            console.log(numAlt);
            this._router.navigate(['/editar-pregunta',this.preguntas[numAlt]._id]);
        }
        GuardarPregunta(numAlt:number){
            console.log(numAlt);
            let id =this.preguntas[numAlt]._id;
            this._preguntaService.editPregunta(this.token,id,this.preguntas[numAlt]).subscribe(
            response=>{
                console.log(response);
                if(!response.pregunta){
                    alert("Error en el servidor");
                    this.alertMessage="Error en el servidor";
                }else{
                    this.pregunta=response.pregunta;
                    console.log(this.pregunta);
                    // this._router.navigate(['/editar-artista',response.artist._id]);
                    this.alertMessage="La pregunta se ha actualizado correctamente";
                }
            },
            error=>{
                    var errorMessage=<any>error;
                    if(errorMessage!=null){
                        var body=JSON.parse(error._body);
                        // this.errorMessage=body.message;
                        console.log(error);
                    }
            });

        }




}