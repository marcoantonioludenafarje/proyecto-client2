import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormsModule} from '@angular/forms';
import{HttpModule} from '@angular/http';
import{routing,appRoutingProviders} from './app.routing';
import { AppComponent } from './app.component';
import{UserEditComponent} from './components/user-edit.component';
import{HomeComponent} from './components/home.component';
import {UsandoLibreriaComponent} from './components/usandoLibreria.component';
import{ArtistListComponent} from './components/artist-list.component';
import{ArtistAddComponent} from './components/artist-add.component';
import{ArtistEditComponent} from './components/artist-edit.component';
import{ArtistDetailComponent} from './components/artist-detail.component';
import{AlbumAddComponent} from './components/album-add.component';
import{AlbumEditComponent} from './components/album-edit.component';

import{MisCarrerasComponent} from './components/misCarreras.component';
import{MisCarrerasDetailComponent} from './components/miCarrera-detail.component';
import{ClaseCarreraExplorarComponent} from './components/claseCarreraExplorar.component';
import{MisTestComponent} from './components/mis-test.component';
import{AgregarPreguntaComponent} from './components/agregar-pregunta.component';
import{ConfigurarTestComponent} from './components/configurar-test.component';
import{EditarPreguntaComponent} from './components/editar-pregunta.component';
import{MisTestOficialComponent} from './components/mis-test-oficial.component';
import{AgregarPersonalidadComponent} from './components/agregar-personalidad.component';
import{PanelControlAlumnosComponent} from './components/panel-control-alumnos.component';
import{AdministracionUsuariosComponent} from './components/administracion-usuarios.component';
// Import the Animations module
// import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GridModule } from '@progress/kendo-angular-grid';
import { Jsonp, JsonpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { EditService } from './services/edit.service';


@NgModule({
  declarations: [
    AppComponent,
    UserEditComponent,
    UsandoLibreriaComponent,
    ArtistListComponent,
    ArtistAddComponent,
    ArtistEditComponent,
    HomeComponent,
    ClaseCarreraExplorarComponent,
    MisTestComponent,
    ArtistDetailComponent,
    AlbumAddComponent,
    AlbumEditComponent,
    AgregarPreguntaComponent,
    ConfigurarTestComponent,
    EditarPreguntaComponent,
    MisCarrerasComponent,
    MisCarrerasDetailComponent,
    MisTestOficialComponent,
    AgregarPersonalidadComponent,
    PanelControlAlumnosComponent,
    AdministracionUsuariosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    // Register the modules
      JsonpModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        GridModule

  ],
  providers: [appRoutingProviders,{ provide: 'Window',  useValue: window }],
  bootstrap: [AppComponent]
})
export class AppModule {}
