import {Personalidad} from '../models/personalidad';


export var Personalidades=[
    new Personalidad("Realista",
    "Prefieren trabajar con objetos o máquinas. En general son personas prácticas y persistentes, con capacidades mecánicas y que prefierentrabajar en el exterior.",
    0,
    [{nombre:"Ingenieria de Sistemas"},{nombre:"Ingenieria Industrial"}],
    {primario:"rgb(125, 189, 202)",secundario:"rgb(97, 103, 153)"}),
    
    new Personalidad("Investigador",
    "Estas personas prefieren trabajar con ideas. En general son analíticas y reservadas, con capacidades científicas y matemáticas. ",
    1,
    [{nombre:"Marketing"},{nombre:"Filosofia"}],
    {primario:"rgb(136, 200, 195)",secundario:"rgb(0, 126, 151)"}),

    new Personalidad("Artista",
    "Prefieren trabajar con ideas creativas, así como con las distintas formas de expresarlas y darlas a conocer a los demás. En general son personas emotivas y abiertas.",
    2,
    [{nombre:"Marketing"},{nombre:"Filosofia"}],
    {primario:"rgba(255, 195, 113, 0.61)",secundario:"#FF9800"}),

    new Personalidad("Social",
    "Prefieren trabajar e interactuar con personas, en general. Son personas serviciales y amistosas,que prefieren trabajar en áreas que les permitan aconsejar, orientar y enseñar",
    3,
    [{nombre:"Marketing"},{nombre:"Filosofia"}],
    {primario:"rgba(228, 159, 139, 0.6)",secundario:"#d63812"}),
    
    new Personalidad("Emprendedor",
    "Estas personas prefieren conducir o dirigir personas. En general son sociables y audaces. Muestran capacidades de liderazgo y comunicación.",
    4,
    [{nombre:"Marketing"},{nombre:"Filosofia"}],
    {primario:"rgb(234, 163, 119)",secundario:"rgb(177, 15, 56)"}),
    
    new Personalidad("Convencional",
    "Prefieren organizar y manejar datos, trabajando en ambientes en donde se requiera la sistematización de la información. Por lo general son personas metódicas y prácticas.",
    5,
    [{nombre:"Marketing"},{nombre:"Filosofia"}],
    {primario:"rgba(142, 36, 170, 0.6)",secundario:"rgb(69, 39, 160)"}),
    
    
];
