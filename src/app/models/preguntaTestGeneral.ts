export class SkillBasico{
skillBasico:string;
puntos:number
}
export class Alternativas{
valorSkills:SkillBasico[];
ordenAlternativa:number;
cuerpoAlternativa:string
}
export class PreguntaTestGeneral{
    
        public alternativas:Alternativas[];
        public numeroPregunta:number;
        public criterio:string;
        public esCarta:boolean;
        public _id:string;
        public fechaCreacion:Date;
        public fechaActualizacion:Date;

    constructor(
        public cuerpoPregunta:string,
        public tipoPregunta:string
        // public alternativas:[{
        //         cuerpoAlternativa:string,
        //         suma:[{skillBasico:string,puntos:number}],
        //         }]
    ){}

}