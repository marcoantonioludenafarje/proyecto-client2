import{TestResultado,PuntajeSkills} from '../models/testResultado';

export class User{
    public cuentanosDeTi:Object;
    // public puntajeSkills:PuntajeSkills[];
    public testResultado:TestResultado;
    constructor(
        public _id:string,
        public name:string,
        public surname:string,
        public email:string,
        public password:string,
        public role:string,
        public image:string
    ){}

}