export class Carrera{
    nombre:String
}

export class Personalidad{
    public puntaje:number
    constructor(
        public nombre:string,
        public descripcion:string,
        public num:number,
        public carreras:[{nombre:String}],
        public color:{
            primario:String,
            secundario:String
        }
    ){}

}